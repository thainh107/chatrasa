


1. Install venv a virtual environment (strongly recommended): 
    Linux: $ sudo apt update
            $ sudo apt install python3-dev python3-pip

            Create a new virtual environment by choosing a Python interpreter and making a ./venv directory to hold it:
            $ python3 -m venv --system-site-packages ./venv

            Activate the virtual environment:
            $ source ./venv/bin/activate

    Window: pip3 install -U pip
        Create a new virtual environment by choosing a Python interpreter and making a .\venv directory to hold it:
        C:\> python3 -m venv --system-site-packages ./venv

        Activate the virtual environment:
        C:\> .\venv\Scripts\activate
    All OS:
        Install Rasa Open Source
        First make sure your pip version is up to date:
        $ pip install -U pip

        To install Rasa Open Source:
        $ pip install rasa

2. Rasa:
    $ rasa train

    Install library get rss
    $ pip install feedparser

3. 