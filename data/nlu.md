## intent:greet
- hey
- hello
- hi
- hey there
- hi there
- hello there
- hey bot
- good morning
- goodmorning
- hello
- goodevening
- goodafternoon
- good evening
- morning
- good afternoon
- Hi
- Hey
- Hi bot
- Hey bot
- Hello
- Good morning
- hi again
- hi folks
- hi Mister
- hi pal!
- hi there
- greetings
- hello everybody
- hello is anybody there
- hello robot
- hallo
- heeey
- hi hi
- hey
- hey hey
- hello there
- hi
- hello
- yo
- hola
- hi?
- hey bot!
- hello friend

## intent:goodbye
- bye
- goodbye
- see you around
- see you later

## intent:affirm
- yes
- indeed
- of course
- that sounds good
- correct
- uh yes im looking for a cheap restaurant in the west part of town
- yeah a cheap restaurant serving international food
- correct
- ye
- uh yes restaurant that serves danish food
- let's do it
- yeah
- yes that sells korean food
- yes can i have can i get swedish food in any area
- yes id like an expensive restaurant in the east part of town
- uh yes a cheap restaurant
- yes that serves korean food
- um yes
- yes knocking
- yes italian food
- yes in the moderately priced
- thats correct gastropub food
- uh yes im looking for a cheap restaurant that serves medetanian food
- yes yes
- uh yes can i find a restaurant in the east part of town that serves chinese
- uh yes im looking for malaysian food
- right
- yea
- yes
- yes i am in the east part of town right now and i am looking for any place that serves indonesian food
- yes south part of town
- yes right
- yes and i dont care about the price range
- yeah i need to find
- uh yes i need the north part of town
- uh yeah im looking for a restaurant in the south part of town and that serves kosher food
- yea thank you good bye
- yes can i have
- yes and in french please
- uh yes can i find a moderately priced restaurant and it serve it should serve brazilian food
- right on good bye peace
- yes in the west part of town
- yes barbecue food
- i love that
- yes spanish restaurant

## intent:deny
- no
- never
- I don't think so
- don't like that
- no way
- not really
- no danish food
- no north
- no
- no new selection
- no im looking for pan asian
- no thanks
- no i want american food
- no thank you good bye
- no thank you
- no spanish food
- no im looking in the south of the town
- no indian food
- uh no
- no american food
- no the south part
- oh no and cheap
- no spanish
- no british food
- no south part of town
- no im looking for one that serves vietnamese food
- do you have something else
- no chinese
- no i want halal food
- no hungarian food
- no center
- no this does not work for me
- no thai

## intent:mood_great
- perfect
- very good
- great
- amazing
- wonderful
- I am feeling very good
- I am great
- I'm good

## intent:mood_unhappy
- sad
- very sad
- unhappy
- bad
- very bad
- awful
- terrible
- not very good
- extremely sad
- so sad

## intent:bot_challenge
- are you a bot?
- are you a human?
- am I talking to a bot?
- am I talking to a human?

## intent:ask_lottery
- Cho anh xem ket qua de
- xem ket qua
- cho anh hoi ket qua
- co ket qua chua
- ket qua
- xo so
- ket qua xo so
- kqxs
- kq
- xs
- ketqua
- lottery